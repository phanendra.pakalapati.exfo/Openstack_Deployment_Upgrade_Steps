# Upgrade from Victoria to Xena using Kolla Ansible

## Upgrade Steps

### Create a virtual environment and install necessary packages
```bash
python3 -m venv xena_env
source xena_env/bin/activate
pip install -U pip
pip install ansible-core==2.11.12 ansible==4.10.0 
pip install --upgrade git+https://opendev.org/openstack/kolla-ansible@xena-em

# Prepare for upgrade

cd /etc/kolla
vi globals.yml  # Update configuration as needed, updated openstack release to xena.
kolla-ansible install-deps     # This step did not work but we can ignore warnings/errors as the dependency is passed during upgrade.
cp /etc/kolla/passwords.yml passwords.yml.old
cp xena_env/share/kolla-ansible/etc_examples/kolla/passwords.yml passwords.yml.new
kolla-genpwd -p passwords.yml.new
kolla-mergepwd --old passwords.yml.old --new passwords.yml.new --final /etc/kolla/passwords.yml

# Pull Xena Docker images and perform prechecks

kolla-ansible -i ./all-in-one pull  # Pulls all Xena images
sudo docker images                  # Check all newly pulled images
kolla-ansible -i ./all-in-one prechecks

# Upgrade OpenStack services

kolla-ansible -i ./all-in-one upgrade

# Check Docker containers after upgrade
sudo docker ps

# Notes
# Make sure to review and adjust configurations based on specific deployment needs.
# Verify all the container versions after successful upgrade step.


