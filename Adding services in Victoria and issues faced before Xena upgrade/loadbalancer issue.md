# Upgrade Task from Victoria to Xena - Load Balancer Issue

## Load Balancer Error Encountered

fatal: [s031]: FAILED! => {"changed": false, "msg": "Inventory's group loadbalancer does not exist or it is empty. Please update inventory, as haproxy group was renamed to loadbalancer in the Xena release."}


## Issue Resolution
# To resolve the load balancer issue, the following entry was added to the `globals.yml` file, which was referred for OPS08 configuration:

```yaml
[loadbalancer:children]
Network

# By adding this entry, the load balancer issue was resolved, allowing the upgrade task to proceed successfully.