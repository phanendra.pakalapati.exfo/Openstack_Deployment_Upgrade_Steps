# Chrony Service Setup

## Cleaning Up Chrony Service from Deployed Victoria Configuration
To remove the Chrony service from the deployed Victoria configuration, the following steps were taken:

1. Execute Chrony Cleanup Command:
    ```bash
    kolla-ansible chrony-cleanup -i multinode
    ```

2. Check Docker Containers:
    ```bash
    docker ps
    ```

## Setting Up Chrony as a Systemd Service
To set up Chrony as a Systemd service, the following steps were followed:

1. Install Chrony:
    ```bash
    sudo apt-get install chrony
    ```

2. Back Up Chrony Configuration File:
    ```bash
    cp /etc/chrony/chrony.conf /etc/chrony/chrony.conf_bk
    ```

3. Make Required Configuration Changes:
    ```bash
    vi /etc/chrony/chrony.conf
    ```

4. View Chrony Configuration:
    ```bash
    cat /etc/chrony/chrony.conf
    ```

5. Start Chrony Service:
    ```bash
    sudo systemctl start chrony
    ```

6. Enable Chrony Service to Start on Boot:
    ```bash
    sudo systemctl enable chrony
    ```

7. Check Chrony Service Status:
    ```bash
    systemctl status chrony
    ```

By following these steps, Chrony was set up as a Systemd service, replacing the previously deployed Chrony service in the Victoria configuration.
