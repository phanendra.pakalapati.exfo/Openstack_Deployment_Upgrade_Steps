# Configuration Details for OPS05 Services (Victoria Version)

Below are the configuration details added to the globals.yml file to replicate the OPS05 services in the Victoria version.

```yaml
kolla_base_distro: "ubuntu"
kolla_install_type: "source"
openstack_release: "victoria"
kolla_internal_vip_address: "10.35.1.189"
network_interface: "enp1s0f0"
neutron_external_interface: "enp1s0f1"

enable_cinder: "yes"
enable_cinder_backup: "yes"
enable_cinder_backend_lvm: "yes"
external_ceph_cephx_enabled: "yes"
ceph_glance_keyring: "ceph.client.admin.keyring"
ceph_glance_user: "glance.lab"
#ceph_glance_pool_name: "ceph-lab-images"
glance_backend_ceph: "yes"
glance_backend_file: "no"
cinder_backend_ceph: "yes"
cinder_backup_driver: "ceph"
skip_cinder_backend_check: true
enable_magnum: "yes"
enable_barbican: "yes"

enable_murano: "yes"
enable_octavia: "yes"
octavia_auto_configure: yes
octavia_ca_cert: "/root/server_ca.cert.pem"
octavia_ca_key: "/root/server_ca.key.pem"
octavia_client_cert: "/root/client.cert-and-key.pem"
octavia_client_ca_cert: "/root/client_ca.cert.pem"

