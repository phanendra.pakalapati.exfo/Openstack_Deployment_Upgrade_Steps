# Error Faced while Adding Octavia Service

## Error Details
When attempting to add the Octavia service, the following errors were encountered during certificate file checks:
- `client.cert-and-key.pem`: File does not exist.
- `client_ca.cert.pem`: File does not exist.
- `server_ca.cert.pem`: File does not exist.
- `server_ca.key.pem`: File does not exist.

## Resolution Steps
Certificates were deployed on the controller node using the following steps:
1. Generate server CA certificate and key:
    ```bash
    openssl req -new -x509 -days 365 -nodes -out server_ca.cert.pem -keyout server_ca.key.pem
    ```

2. Generate client certificate and key:
    ```bash
    openssl req -new -nodes -out client.cert-and-key.pem -keyout client.cert-and-key.pem
    ```

3. Sign client certificate with server CA:
    ```bash
    openssl x509 -req -days 365 -in client.cert-and-key.pem -CA server_ca.cert.pem -CAkey server_ca.key.pem -CAcreateserial -out client.cert-and-key.pem
    ```

4. Create client CA certificate:
    ```bash
    openssl x509 -in server_ca.cert.pem -out client_ca.cert.pem
    ```

5. Copy the new certificates to the default path for Octavia:
    ```bash
    cp -p client_ca.cert.pem client.cert-and-key.pem server_ca.cert.pem server_ca.cert.srl server_ca.key.pem /etc/kolla/config/octavia/
    ```

## Testing
To verify the steps and ensure the certificates are correctly deployed, run prechecks for the upgrade:
```bash
kolla-ansible -i multinode prechecks -v
