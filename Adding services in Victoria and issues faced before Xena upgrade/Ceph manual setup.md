# Ceph Manual Setup on Storage Node

Before adding the storage node to the Kolla configuration for Victoria deployment, it's recommended to set up the Ceph cluster with appropriate configurations. Below are the steps followed on the node (s031) to set up Ceph storage.

1. Check disk partitions:
    ```bash
    fdisk -l
    ```

2. Partition the disk (/dev/sdb):
    ```bash
    fdisk /dev/sdb
    ```

3. Print partition details:
    ```bash
    parted /dev/sdb print
    ```

4. Create OSDs from the partitions:
    ```bash
    sudo ceph-volume lvm create --bluestore --data /dev/sdb1
    sudo ceph-volume lvm create --bluestore --data /dev/sdb2
    ```

5. Update package manager:
    ```bash
    sudo apt-get update
    ```

6. Install required packages:
    ```bash
    sudo apt-get install -y lsb-release gnupg
    ```

7. Add Ceph repository key:
    ```bash
    wget -q -O- 'https://download.ceph.com/keys/release.asc' | sudo apt-key add -
    ```

8. Add Ceph repository:
    ```bash
    echo deb https://download.ceph.com/debian-nautilus/ $(lsb_release -sc) main | sudo tee /etc/apt/sources.list.d/ceph.list
    ```

9. Update package manager again:
    ```bash
    sudo apt-get update
    ```

10. Install Ceph:
    ```bash
    sudo apt-get install -y ceph
    ```

11. Prepare disk partitions for OSD:
    ```bash
    fdisk /dev/sdb
    ceph-deploy osd prepare --fs-type ext4 s031:/dev/sdb1 s031:/dev/sdb2
    ```

12. Install ceph-deploy:
    ```bash
    sudo apt-get install ceph-deploy
    ```

13. Create OSD with XFS file system:
    ```bash
    ceph-deploy osd create --fs-type xfs s031:/dev/sdb1:/dev/sdb2
    ```

14. Install Ceph on the node:
    ```bash
    sudo apt-get install ceph
    ceph-deploy install s031
    ```

15. Create initial monitor:
    ```bash
    ceph-deploy mon create-initial
    ```

16. Prepare OSDs:
    ```bash
    ceph-deploy osd prepare s031:/dev/sdb1
    ceph-deploy osd create s031:/dev/sdb1
    ceph-deploy osd create --data /dev/sdb1 s031
    ceph-deploy osd create --data /dev/sdb2 s031
    ```

17. Check Ceph status:
    ```bash
    ceph -s
    ```

18. Activate OSD:
    ```bash
    ceph-deploy osd activate s031:/dev/sdb1:/dev/sd2
    ```

19. Configure Ceph admin:
    ```bash
    ceph-deploy admin s031
    ```

20. Check Ceph status:
    ```bash
    ceph -s
    ```

21. View OSD tree:
    ```bash
    ceph osd tree
    ```
