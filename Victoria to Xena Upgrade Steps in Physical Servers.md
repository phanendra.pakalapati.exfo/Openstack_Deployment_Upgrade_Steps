# Victoria to Xena Upgrade Steps in Physical Servers

Note: The following steps outline the process of upgrading from Victoria to Xena on physical servers. It involves deploying packages on all deployment nodes and performing pull/prechecks/upgrade specifically on the controller node.

1. Install Ansible versions:
    ```bash
    pip install ansible-core==2.11.12 ansible==4.10.0
    ```

2. Check installed packages:
    ```bash
    pip list
    ```

3. Verify Ansible version:
    ```bash
    ansible --version
    ```

4. Upgrade Kolla-Ansible to Xena version:
    ```bash
    pip install --upgrade git+https://opendev.org/openstack/kolla-ansible@xena-em
    ```

5. Navigate to Kolla directory:
    ```bash
    cd /etc/kolla
    ```

6. Edit globals.yml file to set openstack_release to "xena":
    ```bash
    vi globals.yml
    ```

7. Install dependencies:
    ```bash
    kolla-ansible install-deps
    ```

8. Back up existing passwords.yml:
    ```bash
    cp /etc/kolla/passwords.yml passwords.yml.old
    ```

9. Navigate to Kolla-Ansible examples directory:
    ```bash
    cd /usr/local/share/kolla-ansible/etc_examples/kolla
    ```

10. Copy passwords.yml to a new location:
    ```bash
    cp passwords.yml /etc/kolla/passwords.yml.new
    ```

11. Generate new passwords:
    ```bash
    kolla-genpwd -p /etc/kolla/passwords.yml.new
    ```

12. Merge old and new password files:
    ```bash
    kolla-mergepwd --old /etc/kolla/passwords.yml.old --new /etc/kolla/passwords.yml.new --final /etc/kolla/passwords.yml
    ```

13. Navigate back to the home directory:
    ```bash
    cd
    ```

14. Pull Kolla containers:
    ```bash
    kolla-ansible -i ./multinode pull
    ```

15. List Docker images:
    ```bash
    docker images
    ```

16. Perform prechecks:
    ```bash
    kolla-ansible -i ./multinode prechecks
    ```

17. Upgrade Kolla to Xena version:
    ```bash
    kolla-ansible -i ./multinode upgrade -v
    ```

18. List Docker containers:
    ```bash
    docker ps
    ```
