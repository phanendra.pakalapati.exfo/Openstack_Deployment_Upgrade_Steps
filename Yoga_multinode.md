# Yoga OpenStack Release - Multinode Kolla Ansible Deployment Steps

## Below are the steps for Multinode deployment

```bash
# SSH Configuration
ssh-keygen
ssh-copy-id terraform@10.144.224.83       # User ssh key need to copy for all the the deployment nodes to establish proper communication 
ssh-copy-id terraform@10.144.224.84
ssh-copy-id terraform@10.144.224.85
ssh terraform@10.144.224.85

# Environment Setup

mkdir yoga_multinode
sudo apt install git python3-dev libffi-dev gcc libssl-dev python3-venv
python3 -m venv yoga_multinode
source yoga_multinode/bin/activate
pip install -U pip
pip install ansible==5.10.0
pip install git+https://opendev.org/openstack/kolla-ansible@stable/yoga
## Note - The above steps need to be followed in all the deployment nodes and while doing deployment from controller node all the other nodes venv should be in active state (config/deployment step only from controller node)

# Kolla Ansible Configuration

sudo mkdir -p /etc/kolla
sudo chown terraform:terraform /etc/kolla
cp -r yoga_multinode/share/kolla-ansible/etc_examples/kolla/* /etc/kolla
sudo cp yoga_multinode/share/kolla-ansible/ansible/inventory/multinode .
kolla-ansible install-deps
kolla-genpwd
vi /etc/kolla/globals.yml
vi multinode                      # Add all the Openstack components configuration details as per the design plan.

# Docker Setup and Configuration - This step may not be required if there is no errors during prechecks

sudo apt-get install python3-docker  # Fixed Docker SDK task failure issue
sudo usermod -aG docker terraform

# Deployment

kolla-ansible -i ./multinode bootstrap-servers -v
kolla-ansible -i ./multinode prechecks -v
kolla-ansible -i ./multinode deploy -v
sudo docker ps
pip install python-openstackclient
kolla-ansible post-deploy /etc/kolla/admin-openrc.sh
source /etc/kolla/admin-openrc.sh
openstack project list
cd yoga_multinode/share/kolla-ansible
./init-runonce

# Verification

openstack server create --image cirros --flavor m1.tiny --key-name mykey --network demo-net demo1
openstack server list
sudo docker ps
sudo docker images

# Note
# Multinode deployment nodes depends on the configuration we plan, in the above case we are using three nodes as part of multi node deployment.
# The terraform user was used while doing all deployment steps; it can be any username as per your convenience.
# Make sure to review and adjust configurations based on specific deployment needs.
# Verify all the container versions after successful deployment step.

