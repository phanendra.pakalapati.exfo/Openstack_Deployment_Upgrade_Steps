# Yoga OpenStack Release - All-in-One Kolla Ansible Deployment Steps


## Below are the deployment Steps

```bash

# Install required packages

mkdir yoga_multinode
sudo apt install git python3-dev libffi-dev gcc libssl-dev
sudo apt install python3-venv

# Create Virtual Environment

python3 -m venv yoga
source yoga/bin/activate

# Update pip and Install Ansible

pip install -U pip
pip install ansible==5.10.0
pip install ansible-core==2.12.10

# Verify Ansible Installation

ansible --version
pip list

# Install Kolla Ansible for Yoga Release
pip install git+https://opendev.org/openstack/kolla-ansible@stable/yoga
pip list

# Configuration Setup

sudo mkdir -p /etc/kolla
sudo chown terraform:terraform /etc/kolla
cp -r yoga/share/kolla-ansible/etc_examples/kolla/* /etc/kolla
sudo cp yoga/share/kolla-ansible/ansible/inventory/all-in-one .

# Install Dependencies and Generate Passwords

kolla-ansible install-deps
kolla-genpwd # All passwords will get populated automatically in /etc/kolla/passwords.yml

# Global Configuration Setup

cd /etc/kolla
vi globals.yml

# Server Bootstrap and Prechecks

kolla-ansible -i ./all-in-one bootstrap-servers
kolla-ansible -i ./all-in-one prechecks

# Deployment Process

kolla-ansible -i ./all-in-one deploy
sudo docker images
sudo docker ps

# Post Deployment Verification

pip install python-openstackclient
kolla-ansible post-deploy /etc/kolla/admin-openrc.sh
source /etc/kolla/admin-openrc.sh
openstack token issue
openstack project list

# Final Steps

cd yoga/share/kolla-ansible/
./init-runonce

# Notes
# The terraform user was used while doing all deployment steps; it can be any username as per your convenience.
# Make sure to review and adjust configurations based on specific deployment needs.
# Verify all the container versions after successful deployment step.
