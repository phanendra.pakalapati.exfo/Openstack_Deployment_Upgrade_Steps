# Multinode Victoria Deployment Steps

Below are the steps for deploying a multinode setup using Kolla-Ansible with Victoria version.

## Prerequisites

- Ensure that `python3-pip` is installed:
    ```
    sudo apt install python3-pip
    ```

- Verify installed Python packages:
    ```
    pip list
    ```

- Install Docker:
    ```
    sudo apt install docker.io
    ```

- Check Docker version:
    ```
    docker --version
    ```

- Install required packages for Ansible and Kolla-Ansible:
    ```
    sudo apt install git python3-dev libffi-dev gcc libssl-dev
    ```

- Install Ansible 2.9.27:
    ```
    pip install ansible==2.9.27
    ```

- Verify Ansible installation:
    ```
    ansible --version
    ```

- Install Kolla-Ansible from Victoria branch:
    ```
    pip install git+https://opendev.org/openstack/kolla-ansible@victoria-em
    ```

## Deployment Steps

1. Create necessary directories:
    ```
    mkdir -p /etc/kolla
    ```

2. Copy Kolla-Ansible configuration files to `/etc/kolla` directory:
    ```
    sudo cp -r /usr/local/share/kolla-ansible/etc_examples/kolla/* /etc/kolla
    ```

3. Copy the multinode inventory file:
    ```
    sudo cp /usr/local/share/kolla-ansible/ansible/inventory/multinode .
    ```

4. Update global configurations:
    ```
    sudo vi /etc/kolla/globals.yml
    ```

5. Generate passwords:
    ```
    kolla-genpwd
    ```

6. Modify the multinode inventory file with controller, network, and compute node details:
    ```
    vi multinode
    ```

7. Update `/etc/hosts` file with IP addresses and hostnames:
    ```
    sudo vi /etc/hosts
    ```

8. Configure SSH access:
    - Copy SSH keys to nodes:
        ```
        ssh-copy-id -i /root/.ssh/id_rsa.pub root@<node_IP>
        ```

    - Edit SSH configuration to permit root password:
        ```
        sudo vi /etc/ssh/sshd_config
        ```

    - Restart SSH service:
        ```
        sudo systemctl restart ssh
        ```

9. Bootstrap servers:
    ```
    kolla-ansible -i multinode bootstrap-servers -v
    ```

10. Perform pre-deployment checks:
    ```
    kolla-ansible -i multinode prechecks
    ```

11. Deploy OpenStack services:
    ```
    kolla-ansible -i multinode deploy
    ```

12. Verify deployed containers:
    ```
    docker ps
    ```

13. Install OpenStack client:
    ```
    pip install python-openstackclient
    ```

14. Perform post-deployment tasks:
    ```
    kolla-ansible post-deploy /etc/kolla/admin-openrc.sh
    ```

15. Source admin-openrc.sh file:
    ```
    source /etc/kolla/admin-openrc.sh
    ```

16. Verify OpenStack projects:
    ```
    openstack project list
    ```

17. Initialize and run once:
    ```
    cd /usr/local/share/kolla-ansible
    ./init-runonce
    ```

18. Create a demo instance:
    ```
    openstack server create --image cirros --flavor m1.tiny --key-name mykey --network demo-net demo1
    ```

These steps will guide you through setting up a multinode OpenStack environment using Kolla-Ansible with Victoria version.